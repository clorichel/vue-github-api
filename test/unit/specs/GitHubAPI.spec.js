import Vue from 'vue'
import VueResource from 'vue-resource'
Vue.use(VueResource)
import Vuex from 'vuex'
Vue.use(Vuex)
import GitHubAPI from 'src/GitHubAPI'
Vue.use(GitHubAPI, { token: 'main.js Private Token' })
const store = new Vuex.Store({})
import DevTest from 'src/components/DevTest'

describe('DevTest.vue', () => {
  it('should render correct contents', () => {
    const vm = new Vue({
      el: document.createElement('div'),
      store,
      render: (h) => h(DevTest)
    })
    expect(vm.$el.querySelector('.devtest p strong').textContent)
      .to.equal('Repositories grabbed:')
  })
})
