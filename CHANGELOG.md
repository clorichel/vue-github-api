# Changelog
All notable changes to this project are documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/). For each version listed below, changes are grouped to describe their impact on the project, as follows:

- `Added` for new features
- `Changed` for changes in existing functionality
- `Deprecated` for once-stable features removed in upcoming releases
- `Removed` for deprecated features removed in this release
- `Fixed` for any bug fixes
- `Security` to invite users to upgrade in case of vulnerabilities

## [Unreleased]
### Added
- work in progress on [clorichel/vue-github-api issues](https://gitlab.com/clorichel/vue-github-api/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=Feature)...

## 0.1.7 - 2016-12-05
### Added
- Just bumped to v0.1.7 to mirror vue-gitlab-api releases

## 0.1.0 - 2016-12-05
### Added
- Initial port starting from clorichel/vue-gitlab-api@019f3f28a64b80b13a63e84c12e4955dbbb2683d
- Support to consume GitHub API

[Unreleased]: https://gitlab.com/clorichel/vue-github-api/compare/v0.1.7...master